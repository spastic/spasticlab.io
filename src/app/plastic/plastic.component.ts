import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plastic',
  templateUrl: './plastic.component.html',
  styleUrls: ['./plastic.component.sass']
})
export class PlasticComponent implements OnInit {
  visible: boolean;

  constructor() { }

  ngOnInit() {
    this.visible = false;
  }

  switchVisible() {
    this.visible = !this.visible;
  }

}
